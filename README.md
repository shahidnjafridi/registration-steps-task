## Home Task

### Technologies
- PHP 7.3
- SQlite
- Laravel Framework 8.21.0

### Code files to check
- app/Http/Controllers/*
- app/DataTransferObjects/*
- app/Factories/*
- app/Http/Requests/* (used for server side validation)
- app/Services
- tests/Unit/Services (tests for services)
- resources/views


**1. Describe possible performance optimizations for your Code.**

**Answer:**
There is no major performance issue on backend side, but the issue is with calling an external payment API to get paymentDataId during registration.
It is very possible that api fails and payment data is not saved with error or takes more time than expected (i.e more than 10 seconds). it is not a good idea call an API directly during registration. The best way to dispatch queue message (Rabbit mq / banstalkd) right after user is registered; and the message will be consumed in background that will save paymentDataId for that custome.

**2. Which things could be done better, than you’ve done it?**

**Answer:** 
As mentioned in the first question, i would rather call payment API with a background job. It can be rabbit mq or beanstalkd message which will be triggered right after registration. If job fails it will retry and developer will be informed and customer will not see the error. Another approach will be cron job which runs after every few minutes and get payment daata id for customer with missing payment data.

For this task, i used only table for customer details and payment data. it would be better to split in to different table like customer_payment_data with foreign key to customer.



