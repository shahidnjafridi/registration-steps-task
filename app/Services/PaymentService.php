<?php

namespace App\Services;

use App\DataTransferObjects\PaymentData;
use App\DataTransferObjects\PaymentDataResult;
use App\Exceptions\PaymentException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

class PaymentService
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param PaymentData $paymentData
     * @return PaymentDataResult
     * @throws PaymentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function savePaymentData(PaymentData $paymentData): PaymentDataResult
    {
        $response = $this->httpClient->post(Config::get('services.payment_api.saveDataUrl'), [
            'id' => $paymentData->getCustomerId(),
            'owner' => $paymentData->getAccountHolder(),
            'iban' => $paymentData->getIban()
        ]);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new PaymentException('Error occured during save payment data');
        }

        $result = json_decode($response->getBody(), true);

        return new PaymentDataResult($result['paymentDataId']);
    }
}
