<?php

namespace App\Services;

use App\DataTransferObjects\CustomerData;
use App\DataTransferObjects\PaymentData;
use App\Factories\CustomerFactory;
use App\Models\Customer;

class CustomerDataSaver
{
    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @var PaymentService
     */
    private $paymentService;

    public function __construct(CustomerFactory $customerFactory, PaymentService $paymentService)
    {
        $this->customerFactory = $customerFactory;
        $this->paymentService = $paymentService;
    }

    public function saveCustomerData(CustomerData $customerData): Customer
    {
        $customer = $this->customerFactory->createFromDTO($customerData);
        $customer->save();

         $paymentDataResult = $this->paymentService->savePaymentData(
            new PaymentData(
                $customer->getId(),
                $customer->getBankAccountHolder(),
                $customer->getBankAccountIban()
            )
        );

        $customer->setPaymentId($paymentDataResult->getPaymentDataId());
        $customer->save();

        return $customer;
    }
}
