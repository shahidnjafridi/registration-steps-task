<?php

namespace App\DataTransferObjects;

class CustomerData
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $telephone;

    /**
     * @var string
     */
    private $street;

    /**
     * @var int
     */
    private $streetNumber;

    /**
     * @var string
     */
    private $city;

    /**
     * @var int
     */
    private $postalCode;

    /**
     * @var string
     */
    private $bankAccountHolder;

    /**
     * @var string
     */
    private $bankAccountIban;

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return CustomerData
     */
    public function setFirstName(string $firstName): CustomerData
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return CustomerData
     */
    public function setLastName(string $lastName): CustomerData
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return CustomerData
     */
    public function setTelephone(string $telephone): CustomerData
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return CustomerData
     */
    public function setStreet(string $street): CustomerData
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return int
     */
    public function getStreetNumber(): int
    {
        return $this->streetNumber;
    }

    /**
     * @param int $streetNumber
     * @return CustomerData
     */
    public function setStreetNumber(int $streetNumber): CustomerData
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return CustomerData
     */
    public function setCity(string $city): CustomerData
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return int
     */
    public function getPostalCode(): int
    {
        return $this->postalCode;
    }

    /**
     * @param int $postalCode
     * @return CustomerData
     */
    public function setPostalCode(int $postalCode): CustomerData
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountHolder(): string
    {
        return $this->bankAccountHolder;
    }

    /**
     * @param string $bankAccountHolder
     * @return CustomerData
     */
    public function setBankAccountHolder(string $bankAccountHolder): CustomerData
    {
        $this->bankAccountHolder = $bankAccountHolder;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountIban(): string
    {
        return $this->bankAccountIban;
    }

    /**
     * @param string $bankAccountIban
     * @return CustomerData
     */
    public function setBankAccountIban(string $bankAccountIban): CustomerData
    {
        $this->bankAccountIban = $bankAccountIban;

        return $this;
    }
}
