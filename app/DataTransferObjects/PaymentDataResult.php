<?php

namespace App\DataTransferObjects;

class PaymentDataResult
{
    /**
     * @var string
     */
    private $paymentDataId;

    public function __construct(string $paymentDataId)
    {
        $this->paymentDataId = $paymentDataId;
    }

    /**
     * @return string
     */
    public function getPaymentDataId(): string
    {
        return $this->paymentDataId;
    }
}
