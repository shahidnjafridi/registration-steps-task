<?php

namespace App\DataTransferObjects;

class PaymentData
{
    /**
     * @var string
     */
    private $iban;

    /**
     * @var string
     */
    private $accountHolder;

    /**
     * @var string
     */
    private $customerId;

    public function __construct(int $customerId, string $accountHolder, string $iban)
    {
        $this->customerId = $customerId;
        $this->accountHolder = $accountHolder;
        $this->iban = $iban;
    }

    /**
     * @return string
     */
    public function getIban(): string
    {
        return $this->iban;
    }

    /**
     * @return string
     */
    public function getAccountHolder(): string
    {
        return $this->accountHolder;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }
}
