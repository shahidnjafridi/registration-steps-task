<?php

namespace App\Factories;

use App\DataTransferObjects\CustomerData;
use App\Models\Customer;

class CustomerFactory
{
    public function createFromDTO(CustomerData $customerData): Customer
    {
        return new Customer([
            'first_name' => $customerData->getFirstName(),
            'last_name' => $customerData->getLastName(),
            'telephone' => $customerData->getTelephone(),
            'street' => $customerData->getStreet(),
            'street_number' => $customerData->getStreetNumber(),
            'city' => $customerData->getCity(),
            'postal_code' => $customerData->getPostalCode(),
            'bank_account_iban' => $customerData->getBankAccountIban(),
            'bank_account_holder' => $customerData->getBankAccountHolder(),
        ]);
    }
}
