<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * @var bool
     */
    protected static $unguarded = true;

    /**
     * @param string $paymentId
     * @return $this
     */
    public function setPaymentId(string $paymentId): self
    {
        $this->payment_id = $paymentId;

        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBankAccountHolder() : string
    {
        return $this->bank_account_holder;
    }

    /**
     * @return string
     */
    public function getBankAccountIban(): string
    {
        return $this->bank_account_iban;
    }
}
