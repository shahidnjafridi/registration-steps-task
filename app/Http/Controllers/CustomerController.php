<?php

namespace App\Http\Controllers;

use App\DataTransferObjects\CustomerData;
use App\Http\Requests\AddressInfoRequest;
use App\Http\Requests\BasicInfoRequest;
use App\Http\Requests\PaymentInfoRequest;
use App\Services\CustomerDataSaver;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
    /**
     * @var CustomerDataSaver
     */
    private $customerDataSaver;

    public function __construct(CustomerDataSaver $customerDataSaver)
    {
        $this->customerDataSaver = $customerDataSaver;
    }

    /**
     * @param Request $request
     * @return View|RedirectResponse
     */
    public function registerStepOne(Request $request)
    {
        return $this->getStepView($request, 'customer.register-step-one');
    }

    /**
     * @param BasicInfoRequest $request
     * @return RedirectResponse
     */
    public function postRegisterStepOne(BasicInfoRequest $request): RedirectResponse
    {
        $customerDTO = $this->getCustomer($request);
        $customerDTO->setFirstName($request->get('first_name'))
            ->setLastName($request->get('last_name'))
            ->setTelephone($request->get('telephone'));

        $this->saveCustomerData($request, $customerDTO);

        return $this->saveAndRedirectRoute($request, 'customer.register.step.two');
    }

    /**
     * @param Request $request
     * @return View|RedirectResponse
     */
    public function registerStepTwo(Request $request)
    {
        return $this->getStepView($request, 'customer.register-step-two');
    }

    /**
     * @param AddressInfoRequest $request
     * @return RedirectResponse
     */
    public function postRegisterStepTwo(AddressInfoRequest $request): RedirectResponse
    {
        $customerDTO = $this->getCustomer($request);

        $customerDTO->setStreet($request['street'])
            ->setStreetNumber($request['street_number'])
            ->setCity($request['city'])
            ->setPostalCode($request['postal_code']);

        $this->saveCustomerData($request, $customerDTO);

        return $this->saveAndRedirectRoute($request, 'customer.register.step.three');
    }

    /**
     * @param Request $request
     * @return View|RedirectResponse
     */
    public function registerStepThree(Request $request)
    {
        return $this->getStepView($request, 'customer.register-step-three');
    }

    /**
     * @param PaymentInfoRequest $request
     * @return RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function postRegisterStepThree(PaymentInfoRequest $request): RedirectResponse
    {
        $customerDTO = $this->getCustomer($request);

        $customerDTO->setBankAccountHolder($request['bank_account_holder'])
            ->setBankAccountIban($request['bank_account_iban']);

        try {
            $customer = $this->customerDataSaver->saveCustomerData($customerDTO);

            return $this->saveAndRedirectRoute($request, 'customer.register.success', [$customer->getId()]);
        } catch (\Exception $exception) {
            return $this->saveAndRedirectRoute($request, 'customer.register.step.one')
                ->withErrors('Error:'.$exception->getMessage());
        }
    }

    /**
     * @param Customer $customer
     * @param Request $request
     * @return View|RedirectResponse
     */
    public function registerSuccess(Customer $customer, Request $request)
    {
        $savedRoute = $request->session()->get('last_route');
        if ($savedRoute !== $request->route()->getName()) {
            return redirect('/');
        }

        $request->session()->flush();

        return view('customer.register-success', ['customer' => $customer]);
    }

    /**
     * @param Request $request
     * @param string $viewTemplateName
     * @param array $viewData
     * @return View|RedirectResponse
     */
    private function getStepView(Request $request, string $viewTemplateName, array $viewData = [])
    {
        $savedRoute = $request->session()->get('last_route', 'customer.register.step.one');
        if ($savedRoute !== $request->route()->getName()) {
            return redirect()->route($savedRoute);
        }

        return view($viewTemplateName, $viewData);
    }

    /**
     * @param Request $request
     * @return CustomerData
     */
    private function getCustomer(Request $request): CustomerData
    {
        return $request->session()->get('customer', new CustomerData());
    }

    /**
     * @param Request $request
     * @param CustomerData $customerDTOData
     */
    private function saveCustomerData(Request $request, CustomerData $customerDTOData): void
    {
        $request->session()->put('customer', $customerDTOData);
    }

    /**
     * @param Request $request
     * @param string $name
     * @param array $paremeters
     * @return RedirectResponse
     */
    private function saveAndRedirectRoute(Request $request, string $name, array $paremeters = []): RedirectResponse
    {
        $request->session()->put('last_route', $name);

        return redirect()->route($name, $paremeters);
    }
}
