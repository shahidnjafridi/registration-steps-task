<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'street' => 'required',
            'street_number' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
        ];
    }
}
