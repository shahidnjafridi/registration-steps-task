@extends('layout.default')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form action="{{ route('customer.register.step.one.post') }}" method="POST">
                    @csrf

                    <div class="card">
                        <div class="card-header">Step 1: Basic Info</div>

                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="title">First name:</label>
                                <input type="text" value="" class="form-control" id="customerFirstName"  name="first_name" required/>
                            </div>
                            <div class="form-group">
                                <label for="description">Last name:</label>
                                <input type="text"  value="" class="form-control" id="customerLastName" name="last_name" required/>
                            </div>
                            <div class="form-group">
                                <label for="description">Telephone:</label>
                                <input type="text"  value="" class="form-control" id="customerTelephone" name="telephone" required/>
                            </div>


                        </div>

                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Next</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
