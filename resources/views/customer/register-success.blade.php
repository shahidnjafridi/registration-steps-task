@extends('layout.default')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Data has been added successfully</div>
                    <div class="alert alert-info"><h4>Payment data id: </h4>{{$customer->payment_id}}</div>

                    <div class="card-body">

                        <table class="table">
                            <tr>
                                <td>First Name:</td>
                                <td><strong>{{$customer->first_name}}</strong></td>
                            </tr>
                            <tr>
                                <td>Last Name:</td>
                                <td><strong>{{$customer->last_name}}</strong></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
