@extends('layout.default')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form action="{{ route('customer.register.step.three.post') }}" method="POST">
                    @csrf

                    <div class="card">
                        <div class="card-header">Step 3: Bank account Info</div>

                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="title">Bank account holder:</label>
                                <input type="text" value="" class="form-control" id="customerAccountHolder"  name="bank_account_holder" required/>
                            </div>
                            <div class="form-group">
                                <label for="description">Bank account Iban:</label>
                                <input type="text"  value="" class="form-control" id="customerAccountIban" name="bank_account_iban" required/>
                            </div>

                        </div>

                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Next</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
