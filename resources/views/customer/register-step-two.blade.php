@extends('layout.default')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form action="{{ route('customer.register.step.two.post') }}" method="POST">
                    @csrf

                    <div class="card">
                        <div class="card-header">Step 2: Address Info</div>

                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="title">Street:</label>
                                <input type="text" value="" class="form-control" id="customerStreet"  name="street" required/>
                            </div>
                            <div class="form-group">
                                <label for="description">Street number:</label>
                                <input type="text"  value="" class="form-control" id="customerStreetNumber" name="street_number" required/>
                            </div>

                            <div class="form-group">
                                <label for="description">City:</label>
                                <input type="text"  value="" class="form-control" id="customerCity" name="city" required/>
                            </div>

                            <div class="form-group">
                                <label for="description">Postal code:</label>
                                <input type="text"  value="" class="form-control" id="customerPostalCode" name="postal_code" required/>
                            </div>

                        </div>

                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Next</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
