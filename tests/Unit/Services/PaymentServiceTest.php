<?php

namespace Tests\Unit\Services;

use App\DataTransferObjects\PaymentData;
use App\DataTransferObjects\PaymentDataResult;
use App\Exceptions\PaymentException;
use App\Services\PaymentService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;
use Mockery;

class PaymentServiceTest extends TestCase
{
    const API_URL = 'http://test-url';

    const CUSTOMER_BANK_ACCOUNT_HOLDER = 'test_holder';

    const CUSTOMER_BANK_ACCOUNT_IBAN = 'test_iban';

    const CUSTOMER_ID = 1;

    const PAYMENT_ID = 'c68644153714ca78e4102c7f54747a5a3c1c06be332bd4c2b26e';

    const API_PAYMENT_RESULT = ['paymentDataId' => self::PAYMENT_ID];

    /**
     * @var Mockery|Client
     */
    private $httpClient;

    public function setUp(): void
    {
        $this->httpClient = Mockery::mock(Client::class);

        Config::shouldReceive('get')->with('services.payment_api.saveDataUrl')->andReturn(self::API_URL);
    }

    public function testPaymentDataIsSaved()
    {
        $response = new Response(200, [], json_encode(self::API_PAYMENT_RESULT));
        $paymentData = new PaymentData(
            self::CUSTOMER_ID,
            self::CUSTOMER_BANK_ACCOUNT_HOLDER,
            self::CUSTOMER_BANK_ACCOUNT_IBAN
        );

        $this->httpClient->shouldReceive('post')->with(
            self::API_URL,
            [
                'id' => self::CUSTOMER_ID,
                'owner' => self::CUSTOMER_BANK_ACCOUNT_HOLDER,
                'iban' => self::CUSTOMER_BANK_ACCOUNT_IBAN,
            ]
        )
            ->once()
            ->andReturn($response);

        $customerDataSaver = new PaymentService($this->httpClient);
        $result = $customerDataSaver->savePaymentData($paymentData);

        $this->assertEquals(new PaymentDataResult(self::PAYMENT_ID), $result);
    }

    public function testExceptionIsThrownWhenResponseIsNotOK()
    {
        $response = new Response(404, [], json_encode(self::API_PAYMENT_RESULT));
        $paymentData = new PaymentData(
            self::CUSTOMER_ID,
            self::CUSTOMER_BANK_ACCOUNT_HOLDER,
            self::CUSTOMER_BANK_ACCOUNT_IBAN
        );

        $this->httpClient->shouldReceive('post')->with(
            self::API_URL,
            [
                'id' => self::CUSTOMER_ID,
                'owner' => self::CUSTOMER_BANK_ACCOUNT_HOLDER,
                'iban' => self::CUSTOMER_BANK_ACCOUNT_IBAN,
            ]
        )
            ->once()
            ->andReturn($response);

        $this->expectException(PaymentException::class);

        $customerDataSaver = new PaymentService($this->httpClient);
        $customerDataSaver->savePaymentData($paymentData);
    }
}
