<?php

namespace Tests\Unit\Services;

use App\DataTransferObjects\CustomerData;
use App\DataTransferObjects\PaymentDataResult;
use App\Factories\CustomerFactory;
use App\Models\Customer;
use App\Services\CustomerDataSaver;
use App\Services\PaymentService;
use Mockery;
use Tests\TestCase;

class CustomerDataSaverTest extends TestCase
{
    const CUSTOMER_ID = 1;

    const CUSTOMER_BANK_ACCOUNT_HOLDER = 'test_holder';

    const CUSTOMER_BANK_ACCOUNT_IBAN = 'test_iban';

    const CUSTOMER_PAYMENT_DATA_ID = 'test-payment-id';

    /**
     * @var Mockery|CustomerFactory
     */
    private $customerFactory;

    /**
     * @var Mockery|PaymentService
     */
    private $paymentService;

    public function setUp(): void
    {
        $this->customerFactory = Mockery::mock(CustomerFactory::class);
        $this->paymentService = Mockery::mock(PaymentService::class);
    }

    public function testCustomerDataIsSaved()
    {
        $customerModel = Mockery::mock(Customer::class);
        $customerDTO = new CustomerData();

        $customerModel->shouldReceive('getId')->once()->andReturn(self::CUSTOMER_ID);
        $customerModel->shouldReceive('getBankAccountIban')->once()->andReturn(self::CUSTOMER_BANK_ACCOUNT_IBAN);
        $customerModel->shouldReceive('getBankAccountHolder')->once()->andReturn(self::CUSTOMER_BANK_ACCOUNT_HOLDER);

        $paymentDataResult = new PaymentDataResult(self::CUSTOMER_PAYMENT_DATA_ID);

        $this->customerFactory->shouldReceive('createFromDTO')->with($customerDTO)->andReturn($customerModel);

        $this->paymentService->shouldReceive('savePaymentData')
            ->once()
            ->andReturn($paymentDataResult);

        $customerModel->shouldReceive('setPaymentId')->once()->with(self::CUSTOMER_PAYMENT_DATA_ID);
        $customerModel->shouldReceive('save')->once();

        $customerDataSaver = new CustomerDataSaver($this->customerFactory, $this->paymentService);
        $customerDataSaver->saveCustomerData($customerDTO);
    }
}
