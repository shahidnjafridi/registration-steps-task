<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\CustomerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [CustomerController::class, 'registerStepOne'])->name('customer.register.step.one');
Route::post('/', [CustomerController::class, 'postRegisterStepOne'])->name('customer.register.step.one.post');

Route::get('/step-two', [CustomerController::class, 'registerStepTwo'])->name('customer.register.step.two');
Route::post('/step-two', [CustomerController::class, 'postRegisterStepTwo'])->name('customer.register.step.two.post');

Route::get('/step-three', [CustomerController::class, 'registerStepThree'])->name('customer.register.step.three');
Route::post('/step-three', [CustomerController::class, 'postRegisterStepThree'])->name('customer.register.step.three.post');

Route::get('/success/{customer}', [CustomerController::class, 'registerSuccess'])->name('customer.register.success')
    ->where('id', '[0-9]+');
